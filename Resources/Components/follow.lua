follow = Component:extends
{
	last = nil,
	lastFollow = false,
	speed = 300,
	moving = false,
	spos = nil,
	mpos = nil
}
function follow:__init()
	self.super.__init(self,self)
end
function follow:Start()
	MainCamera():follow(self.GameObject)
	love.graphics.setBackgroundColor({100,100,100,255})
end
function follow:mousepressed(x,y,button)
	print(button)
	if button == "wu" then
		MainCamera().scale = MainCamera().scale * 2
	elseif button == "wd" then
		MainCamera().scale = MainCamera().scale / 2
	end
end
function follow:keypressed(button,b)
	pos = MainCamera():camToWorld(love.mouse.getX(),love.mouse.getY())
	if(button == "0") then self.last = GameObject:new("circle",{CircleCollider:new(false,pos.x,pos.y,20,0),Lighting:new()}) end
	if(button == "1") then self.last = GameObject:new("box",{BoxCollider:new(false,pos.x,pos.y,40,40,0),Lighting:new()}) end
	if(button == "2") then self.last = GameObject:new("light",{PointLight:new(pos.x,pos.y,{math.random(20,125),math.random(20,125),math.random(20,125),255},math.random(400,4000),true,true)}) end
	if(button == "3") then self.last = GameObject:new("light",{PointLight:new(pos.x,pos.y,{math.random(20,125),math.random(20,125),math.random(20,125),255},math.random(900,1000),true,false)}) end
	if(button == "4") then self.last = GameObject:new("light",{PointLight:new(pos.x,pos.y,{math.random(20,125),math.random(20,125),math.random(20,125),255},math.random(900,1000),true,false),BoxCollider:new(false,pos.x,pos.y,40,40,0)}) end
	if(button == "doesnt exist") then
		self.last:Destroy()
		self.last = nil
	end
	if(button == "m") then
		FindScene("testScene3"):load()
	end
end
function follow:update(dt)
	pos = MainCamera():camToWorld(love.mouse.getX(),love.mouse.getY())
	if(love.mouse.isDown("r") and self.last ~= nil) then
		self.last.Transform.position = pos;
	end
	if(love.mouse.isDown("x1")) then
		if(math.random(0,1) == 1) then
			self.last = GameObject:new("box",{BoxCollider:new(false,pos.x,pos.y,40,40,0),Lighting:new()})
		else
			self.last = GameObject:new("box",{CircleCollider:new(false,pos.x,pos.y,20,0),Lighting:new()})
		end
	end
	
	if(self.moving == false) then
		if(love.mouse.isDown("l")) then
			self.moving = true
			self.spos = pos
			self.mpos = self.Transform.position
		end
	else
		slope = pos:slope(self.spos)
		if(love.mouse.isDown("l")) then
			
			
		else
			self.Transform.position = self.mpos:add(slope)
			self.moving = false
		end
	end
end
