
spinner = Component:extends
{
	scale = 1
}
function spinner:__init(scale)
	self.super.__init(self,self)
	self.scale = scale or 1
	
end
function spinner:Start()
	if self.scale == 1 then
	self.Transform.z = 10
	end
end
function spinner:update(dt)
	if self.scale == 1 then self.Transform.rot = self.Transform.rot - (1*dt)
	else
	self.Transform.rot = self.Transform.rot + (5*dt)
	end

end
function spinner:draw()
	love.graphics.setColor({255,255,255,255})
	love.graphics.draw(
	Resources.image["/Resources/Art/box.png"],MainCamera():camToWorld(love.mouse.getX(),love.mouse.getY()).x,MainCamera():camToWorld(love.mouse.getX(),love.mouse.getY()).y,self.Transform.rot,self.scale,self.scale,128,128)

end
