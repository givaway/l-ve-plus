z = Component:extends
{
	direction = false,
	speed = 200,
	color = {255,0,0,255}
}
function z:__init(direction)
	self.super.__init(self,self)
	self.direction = direction
	self.color = {math.random(0,255),0,0,255}
end
function z:Start()
	self.Transform.position.y = math.random(-300,300)
	self.Transform.position.x = math.random(-400,400)
	self.Transform.z = self.color[1]
end
function z:update(dt)
	if(self.direction) then
		self.Transform.position.x = self.Transform.position.x + (self.speed*dt)

	else
		self.Transform.position.x = self.Transform.position.x - (self.speed*dt)

	end
	if(self.Transform.position.x > 500 ) then
		self.direction = false
	end
	if(self.Transform.position.x < -500) then
		self.direction = true
	end
end
function z:draw()
	love.graphics.setColor(self.color)
	love.graphics.rectangle("fill",self.Transform.position.x+(math.cos((self.Transform.position.y+(math.sin(self.Transform.position.x/20)*10))/20)*10),self.Transform.position.y+(math.sin(self.Transform.position.x/20)*10),50,50)
end