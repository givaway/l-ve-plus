oldWindows = Component:extends
{
	p1 = nil,
	p2 = nil,
	p3 = nil,
	p21 = nil,
	p22 = nil,
	p23 = nil,
	tpos = 0,
	seed = 0,
	oldColor = {0,0,0,255},
	newColor = {0,0,0,255},
	continue = true,

}
function oldWindows:__init(seed,offset,times)
	self.super.__init(self,self)
	self.offset = offset
	self.tpos = 0
	self.seed = seed or 0
	self.times = times
end
function oldWindows:Start()
	math.randomseed(self.seed)
	self.p1 = Vector2:new(math.random(-635,635),math.random(-360,360))
	self.p2 = Vector2:new(math.random(-635,635),math.random(-360,360))
	self.p3 = Vector2:new(math.random(-635,635),math.random(-360,360))
	self.p21 = Vector2:new(math.random(-635,635),math.random(-360,360))
	self.p22 = Vector2:new(math.random(-635,635),math.random(-360,360))
	self.p23 = Vector2:new(math.random(-635,635),math.random(-360,360))
	self.oldColor = self.oldColor
	self.newColor = {math.random(128,255),math.random(128,255),math.random(128,255),255}
	
end
function oldWindows:newPoint()
	mp = MainCamera():camToWorld(love.mouse.getX(),love.mouse.getY())
	x = mp.x
	y = mp.y
	self.seed = self.seed + 1
	math.randomseed(self.seed)
	self.p1 = self.p21
	self.p2 = self.p22
	self.p3 = self.p23
	self.p21 = Vector2:new(math.random(x-100,x+100),math.random(y-100,y+100))
	self.p22 = Vector2:new(math.random(x-100,x+100),math.random(y-100,y+100))
	self.p23 = Vector2:new(math.random(x-100,x+100),math.random(y-100,y+100))
	self.tpos = 0
		self.oldColor = self.newColor
	self.newColor = {math.random(128,255),math.random(128,255),math.random(128,255),255}
	
end
function oldWindows:update(dt)
	
	
		self.tpos = self.tpos + (1-self.offset)*dt

	if self.tpos > 1 then
		self:newPoint()
	end
	
end
function oldWindows:draw()
love.graphics.setLineWidth( 5 )
	love.graphics.setColor(math.cerp(self.oldColor[1],self.newColor[1],self.tpos),math.cerp(self.oldColor[2],self.newColor[2],self.tpos),math.cerp(self.oldColor[3],self.newColor[3],self.tpos),255)
	love.graphics.triangle("line",
	math.cerp(self.p1.x,self.p21.x,self.tpos),math.cerp(self.p1.y,self.p21.y,self.tpos),
	math.cerp(self.p2.x,self.p22.x,self.tpos),math.cerp(self.p2.y,self.p22.y,self.tpos),
	math.cerp(self.p3.x,self.p23.x,self.tpos),math.cerp(self.p3.y,self.p23.y,self.tpos))
	
end