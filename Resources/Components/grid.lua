grid = Component:extends
{
timer = 0,
size = 15,
sep = -30,
ymax = 20,
wide = 50,
speed = 3,
count = 0,
cnt = 3
}
function grid:__init()

end
function grid:Start()

end
function grid:update(dt)
	self.timer = self.timer + self.speed * dt
	if self.timer > 1 then
	self.timer = 0
	end
	
	
end

function grid:draw()

	math.randomseed(256)
	love.graphics.setLineWidth( 1 )

	for y = 0,self.ymax do
		
		for x = -self.wide,self.wide do
			
			love.graphics.setColor({math.random(100,255),255,255,255})
			love.graphics.polygon("line",x*((self.ymax/(y+self.timer))*self.size),self.sep+(self.ymax/(y+self.timer))*self.size*2,
			(x+1)*((self.ymax/(y+self.timer))*self.size),self.sep+(self.ymax/(y+self.timer))*self.size*2,
			(x+1)*((self.ymax/((y+1)+self.timer))*self.size),self.sep+(self.ymax/((y+self.timer)+1))*self.size*2,
			x*((self.ymax/((y+1)+self.timer))*self.size),self.sep+(self.ymax/((y+self.timer)+1))*self.size*2)
		end
		self.count = self.count + 1
	end
	points = {}
	if self.cnt < 50 then
		if math.timer("grd",550) == true then
			self.cnt = self.cnt + 1
		end
	end
	mp = MainCamera():camToWorld(love.mouse.getX(),love.mouse.getY())
	for i = 0,self.cnt do
		t = i/self.cnt
		table.insert(points,math.bezier(-100,mp.x,mp.x,-100,t))
		table.insert(points,math.bezier(-100,mp.y,-mp.y,100,t))
	end
	love.graphics.polygon("line",points)
	love.graphics.polygon("line",{-100,-100,100,-100,100,100,-100,100})
end