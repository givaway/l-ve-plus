Resources = {}

Resources.image = {}
Resources.imagedata = {}

Resources.audio = {}

Resources.includes = {}
Resources.plugins = {}

Resources.files = {}

Resources.effect = {}
Resources.effectFiles = {}

Resources.fonts = {}
Resources.existingfonts = {}

Resources.animation = {}

Resources.All = {}

Resources.Blacklisted = {}
--Way of Sorting
Resources.extentions = 
{
{"image",".png"},{"image",".jpeg"},{"image",".jpg"},
{"audio",".mp3"},{"audio",".ogg"},{"audio",".wav"},
{"font",".ttf"},
{"shader",".shader"},{"file",".dat"},{"file",".txt"},
{"animation",".anim"},
{"include",".lua"}
}

--blacklisted files
Resources.blacklist = {
"main.lua",
"ResourceLoader.lua",
"conf.lua",
"30log.lua",
"Component.lua",
"GameObject.lua",
"Scene.lua",
"Plus.lua",
"require.lua",
"ResourceLoading.lua",
"LuaBit.lua",
"simplexnoise.lua",
"mainTemplate.lua"
}
Resources.FolderBlacklist = {
	".git" --Just in case you use a git repo
}

--include lua files by default
Resources.include = true
function Resources.load(folder)
	for i,v in ipairs(Resources.FolderBlacklist) do
		if(string.find(folder,v)) then
			return
		end
	end
    local lfs = love.filesystem
    local filesTable = lfs.getDirectoryItems(folder)
    for i,v in ipairs(filesTable) do
        local file = folder.."/"..v
        if lfs.isFile(file) then
			rev1file = string:split(file,'/')
			Resources.sort(rev1file[table.getn(rev1file)],file)
        elseif lfs.isDirectory(file) then
            fileTree = Resources.load(file)
        end
    end
end

--sorts files for use
function Resources.sort(filename,file)
	
	
	local blacklisted = false
	for i,v in ipairs(Resources.blacklist) do
		if filename == v then
			blacklisted = true
			Resources.Blacklisted[filename] = file
			return
		end
	end
	
	local fileext = Resources.getExtension(filename)
	local filetype = nil
	
	for i,v in ipairs(Resources.extentions) do
		if fileext == v[2] or fileext == string.upper(v[2]) then
			filetype = v[1]
			break
		end
	end
	
	if filetype then
		local data = nil
		if filetype == "image" then
			Resources.imagedata[file] = love.image.newImageData( file )
			Resources.image[file] = love.graphics.newImage( Resources.imagedata[file])
			data = Resources.image[file]
		end
		if filetype == "audio" then
			Resources.audio[file] = love.audio.newSource( file, "static" )
			data = Resources.audio[file]
		end
		if filetype == "font" then
			Resources.fonts[filename] = file
			data = file
		end
		if filetype == "file" then
			Resources.files[file] = file
			data = file
		end
		if filetype == "shader" then
			Resources.effect[filename] = file
			Resources.effectFiles[file] = file
			data = file
		end
		if filetype == "animation" then
			Resources.animation[file] = file
			data = file
		end
		if filetype == "include" then
			data = file
			if Resources.isPlugin(file) then
				Resources.plugins[file] = file
			else
				Resources.includes[file] = file
			end
		end
		Resources.All[filename] = data
	end

end
function Resources.isPlugin(str)
	local ret = false
	for line in love.filesystem.lines(str) do
		if string.find(line, "--#plugin",nil,true) ~= nil then
			ret = true
		end
	end
	return ret
end
function Resources.ignore(str)
	local ret = false
	for line in love.filesystem.lines(str) do
		if string.find(line, "--#ignore",nil,true) ~= nil then
			ret = true
		end
	end
	return ret
end
function Resources.include()
	for i,v in pairs(Resources.plugins) do
		if not Resources.ignore(v) then
		require(string:split(v,".")[1])
		end
	end
	for i,v in pairs(Resources.includes) do
		if not Resources.ignore(v) then
		require(string:split(v,".")[1])
		end
	end
end

function Resources.getExtension(str)
	print(str)
	return "."..string:split(str,".")[2];
end

--animation loader
function animationSort(file)
	local anim = {}
	local currentAnim = ""
	for line in love.filesystem.lines(file) do

	  if string.find(line,";",nil,true) then
	  
		if string.find(line,",",nil,true) then
			local newval = string:split(string:split(line,";")[1],",")
			local nvval = assert(loadstring("return {"..table.concat(newval,",").."}"))()
			table.insert(anim[currentAnim],nvval)
		else
			currentAnim = string:split(line,";")[1]
			anim[currentAnim] = {}
		end
	  end
	  
	  
	end
	return anim
end

--fonts classes
	font = class
	{
		name = "",
		tfont = nil,
		size = 0
	}
	function font:__init(fontname,font,size)
		self.name = fontname
		self.tfont = font
		self.size = size
	end
--end
table.insert(Resources.existingfonts,font:new("default",love.graphics.newFont(14),14))

function Resources.createFont(fontname,size)
	local newfont = Resources.getFont(fontname,size)
	if newfont == nil then
		if Resources.fonts[fontname] == nil then return end
		local nf = font:new(fontname,love.graphics.newFont( Resources.fonts[fontname],size),size)
		
		table.insert(Resources.existingfonts,nf)
		newfont = nf
		
	end
end
function Resources.setFont(fontname,size)
	local newfont = Resources.getFont(fontname,size)
	if newfont == nil then
		if Resources.fonts[fontname] == nil then return end
		local nf = font:new(fontname,love.graphics.newFont( Resources.fonts[fontname],size),size)
		
		table.insert(Resources.existingfonts,nf)
		newfont = nf
		
	end
	love.graphics.setFont(newfont.tfont)
end
function Resources.getFont(fontname,size)
	ret = nil
	for i,v in ipairs( Resources.existingfonts) do
		if v.name == fontname and v.size == size then
			ret = v
		end
	end
	return ret
end
effect = nil
function Resources.createPixelEffect(file)
	PixelEffectFile = file
	
	if PixelEffectFile ~= nil then
		PixelEffectCode = ""
		for line in love.filesystem.lines(PixelEffectFile) do
			PixelEffectCode = PixelEffectCode.. line.. "\n"
			
		end

		return love.graphics.newPixelEffect(PixelEffectCode)
	end
	
	
end



function table.equals(table1,table2)
	ret = false
	if table.getn(table1) == table.getn(table2) then 
		local MatchCount = 0
		for i = 1, table.getn(table1) do
			if table1[i] == table2[i] then
				MatchCount = MatchCount + 1
			end
		end
		if MatchCount == table.getn(table1) then
			ret = true
		end
	end
	
	return ret
	
end

function table.shift(tablen,index)
	local reached = false
	for i,v in ipairs(tablen) do
		if reached then
			if v == nil then
			
			else
				tablen[i-1] = v
			end
		else
			if i == index then
				reached = true
			end
		end
	end
end

function math.round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end
function string:split(s, pattern, maxsplit)
  local pattern = pattern or ' '
  local maxsplit = maxsplit or -1
  local s = s
  local t = {}
  local patsz = #pattern
  while maxsplit ~= 0 do
    local curpos = 1
    local found = string.find(s, pattern,nil,true)
    if found ~= nil then
      table.insert(t, string.sub(s, curpos, found - 1))
      curpos = found + patsz
      s = string.sub(s, curpos)
    else
      table.insert(t, string.sub(s, curpos))
      break
    end
    maxsplit = maxsplit - 1
    if maxsplit == 0 then
      table.insert(t, string.sub(s, curpos - patsz - 1))
    end
  end
  return t
end
function string:join(s)
  local t = ""
  for i,v in pairs(s) do 
	t = t..v
  end
  return t
end
function string:ns(s)
  local t = ""
  for i = 1,string.len(s) do
	v = string.sub(s,i,i)
	if v ~= " " or v ~= "\n" then
		t = t..v
	end
  end
  return t
end
function string:trim(s)
  return string.gsub(s, "%s+", "")
end
function table.compare(t1, t2)
  if #t1 ~= #t2 then return false end
  for i=1,#t1 do
    if t1[i] ~= t2[i] then return false end
  end
  return true
end