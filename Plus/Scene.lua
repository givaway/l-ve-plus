--The current scene 
CurrentScene = nil
--Table that has all the scenes in it
Scenes = {}
Scene = class
{
	GameObjects = {},
	name = "",
	object = nil,
	--default camera
	camera = nil,
	--world object
	PhysicsObject = {},
	--lighting
	Lights = {},
	--speed testing
	speed = {},
	speedCalls = {}
}
--[[
--initializes scene class (Would NOT recomment overriding, Core Function )
--]]
function Scene:__init(object)
	table.insert(Scenes,object)
	self.GameObjects = {}
	self.name =  string:split(string:split(debug.getinfo(2).source,'/')[table.getn(string:split(debug.getinfo(2).source,'/'))],'.lua')[1]
	self.object = object
	self:initPhysics(0,0,true)
	math.timers = {}
end
--[[
--initializes Physics (you can override if you want custom x/y gravity or allow sleeping)
--]]
function Scene:initPhysics(x,y,sleep)
	self.PhysicsObject = love.physics.newWorld( x, y, sleep )
	self.PhysicsObject:setCallbacks(beginContact, endContact, preSolve, postSolve)
end

--[[
--Begin speed test
--]]
function Scene:beginSpeedTest(name)
	if self.speedCalls[name] == nil then
		self.speedCalls[name] = 1
	else
		self.speedCalls[name] = self.speedCalls[name] + 1
	end
	if Plus.speedtest then
	if self.speed[name] == nil then
	self.speed[name] = {}
	self.speed[name].name = name
	self.speed[name].hist = {}
	self.speed[name].begin = love.timer.getTime( )
	self.speed[name].endT = nil
	else
	self.speed[name].begin = love.timer.getTime( )
	self.speed[name].endT = nil
	end
	end
end

--[[
--end speed test
--]]
function Scene:endSpeedTest(name)
	if Plus.speedtest then
	self.speed[name].endT = love.timer.getTime( )
	--table.insert(self.speed[name].hist,(self.speed[name].endT-self.speed[name].begin))
	if (self.speed[name].endT-self.speed[name].begin) ~= 0  then
	self.speed[name].avg = (self.speed[name].endT-self.speed[name].begin)
	end
	end
end

--[[
-- use this to load the Scene or to switch to another scene (Would NOT recomment overriding, Core Function )
--]]
function Scene:load()
	InitializingScene = self.object
	self:initPhysics()
	saved = {}
	if self.CurrentScene then
		for i,v in ipairs(CurrentScene.GameObjects) do
			if v.persist then
			table.insert(saved,v);
			print(v.name)
			end
		end
	end
	self.GameObjects = saved
	self.camera = Camera:new()
	CurrentScene = self
	self:Start()
end

--[[
--The Start function should allways be overridden, this is where you declare all your gameobjects
--]]
function Scene:Start()
	
end
--[[
-- default "update" function (Would NOT recomment overriding, Core Function )
--]]

function Scene:update(dt)
	math.update(dt)
	self.PhysicsObject:update(dt)
	self.LightPolys = {}
	for i = 1, table.getn(self.GameObjects) do
	
		if self.GameObjects[i] == nil then
			table.remove(self.GameObjects,i)
		else
		self.GameObjects[i]:update(dt)
		end

	end
end

--[[
-- default "draw" function, also handles layering (Would NOT recomment overriding, Core Function )
--]]
function Scene:preRender()
	for i,v in ipairs(self.GameObjects) do 
			v:preRender()
	end

end
function Scene:postRender()
	for i,v in ipairs(self.GameObjects) do 
			v:postRender()
	end
end
function Scene:draw()
	self:preRender()
	self.camera:open()
	width, height, fullscreen, vsync, fsaa = love.window.getMode()
	local draws = {}
	local indexi = {}
	for i,v in ipairs(self.GameObjects) do 
		local addrad = 0
		local checklight = v:GetComponent("PointLight")
		if checklight ~= nil then
			addrad = checklight.radius
		end
		if (v.Transform.z == "hidden" or v.Transform.position:distance(MainCamera():asVector()) > width/MainCamera().scale+addrad) and v.Transform.zignore == false then
			v.visible = false
		else
			v.visible = true
			if draws[v.Transform.z] == nil then
				draws[v.Transform.z] = {}
				table.insert(draws[v.Transform.z],v)
				table.insert(indexi,v.Transform.z)
				table.sort(indexi)
				
				
			else
				table.insert(draws[v.Transform.z],v)
			end
		end
			
	end
	local drawCount = 0
	for v,i in pairs(indexi) do
		if draws[i] ~= nil then
			for a,b in pairs(draws[i]) do
				b:draw()
				drawCount = drawCount + 1
			end
		end
	end
	
	self.camera:close()
	self:postRender()

	if Plus.debugging then
		local dbuglog = 
		{
		"Debugging",
		"FPS: "..love.timer.getFPS( ),
		"Objects: "..table.getn(self.GameObjects),
		"Draw Calls: "..tostring(drawCount),
		"Layers: "..(table.getn(indexi))
		}
		local prefont = love.graphics.getFont( )
		Resources.setFont("default",14)
		for i,v in pairs(self.speed) do
			love.graphics.setColor({255,255,255,255})
			table.insert(dbuglog,v.name.." x"..self.speedCalls[v.name]..": "..(self.speedCalls[v.name]*v.avg))
		end
		for i,v in ipairs(dbuglog) do
			love.graphics.setColor({255,255,255,255})
			love.graphics.print(v,0,(i-1)*16)
		end
		
		if prefont ~= nil then love.graphics.setFont(prefont) end
	end
	self.speedCalls = {}
end

-- Other Callback Functions
--[[
-- default "mousepressed" function (Would NOT recomment overriding, Core Function )
--]]
function Scene:mousepressed(x, y, button)
		for i = 1, table.getn(self.GameObjects) do
			self.GameObjects[i]:mousepressed(x, y, button)
		end
end
--[[
-- default "mousereleased" function (Would NOT recomment overriding, Core Function )
--]]
function Scene:mousereleased(x, y, button)
		for i = 1, table.getn(self.GameObjects) do
			self.GameObjects[i]:mousereleased(x, y, button)
		end
end
--[[
-- default "keypressed" function (Would NOT recomment overriding, Core Function )
--]]
function Scene:keypressed(key, unicode)
		for i = 1, table.getn(self.GameObjects) do
			self.GameObjects[i]:keypressed(key, unicode)
		end
end
--[[
-- default "keyreleased" function (Would NOT recomment overriding, Core Function )
--]]
function Scene:keyreleased(key, unicode)
		for i = 1, table.getn(self.GameObjects) do
			self.GameObjects[i]:keyreleased(key, unicode)
		end
end
--[[
-- default "focus" function (Would NOT recomment overriding, Core Function )
--]]
function Scene:focus(f)
		for i = 1, table.getn(self.GameObjects) do
			self.GameObjects[i]:focus(f)
		end
end
--[[
-- default "quit" function (Would NOT recomment overriding, Core Function )
--]]
function Scene:quit()
		for i = 1, table.getn(self.GameObjects) do
			self.GameObjects[i]:quit()
		end
end
--[[
-- this function finds any gameobject in the scene with that name (Would NOT recomment overriding, Core Function )
--]]
function Scene:FindGameObject(name)
	ret = nil
	for i = 1, table.getn(self.GameObjects) do
			if self.GameObjects[i].name == name then
				ret = self.GameObjects[i]
			end
	end
	return ret
end
function Scene:DestroyGameObject(name)
	local ret = false
	for i = 1, table.getn(self.GameObjects) do
			if self.GameObjects[i].name == name then
				table.remove(self.GameObjects,i)
				ret = true
				break
			end
	end
	return ret
end
--[[
-- Default Collision Callbacks
--]]
function beginContact(a, b, coll)
   local adat,bdat = a:getUserData(),b:getUserData()
	bdat.GameObj:OnCollisionEnter({adat.GameObj,coll})
	adat.GameObj:OnCollisionEnter({bdat.GameObj,coll})
	adat.touching[bdat.GameObj.name] = true
	bdat.touching[adat.GameObj.name] = true
	a:setUserData(adat)
	b:setUserData(bdat)
   
end

function endContact(a, b, coll)
    local adat,bdat = a:getUserData(),b:getUserData()
	bdat.GameObj:OnCollisionExit({adat.GameObj,coll})
	adat.GameObj:OnCollisionExit({bdat.GameObj,coll})
	adat.touching[bdat.GameObj.name] = nil
	bdat.touching[adat.GameObj.name] = nil
	a:setUserData(adat)
	b:setUserData(bdat)
end

function preSolve(a, b, coll)
	local adat,bdat = a:getUserData(),b:getUserData()
    if adat.touching[bdat.GameObj.name] == nil or bdat.touching[adat.GameObj.name] == nil then
			adat.touching[bdat.GameObj.name] = nil
			bdat.touching[adat.GameObj.name] = nil
    else
        bdat.GameObj:OnCollisionStay({adat.GameObj,coll})
		adat.GameObj:OnCollisionStay({bdat.GameObj,coll})
    end
   
end

function postSolve(a, b, coll)
	
end
--[[
-- finds the any initialized scene with that name (Would NOT recomment overriding, Core Function )
--]]
function FindScene(name)
	ret = nil
	for i,v in ipairs(Scenes) do
		if v.name == name then ret = v end
	end
	return ret
end
--[[
-- returns the current scene camera
--]]
function MainCamera()
	return CurrentScene.camera
end