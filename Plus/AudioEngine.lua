--#ignore
AudioEngine = {}

AudioEngine.files = {}

AudioEngine.listening = {}
AudioEngine.SFX = {}

AudioEngine.Music = {}
AudioEngine.Music.current = nil
AudioEngine.Music.state = nil
AudioEngine.Music.queue = {}
AudioEngine.Music.queuePos = 1


--THE LOOP--

function AudioEngine.update(dt)
	this = AudioEngine
	if this.Music.current then
		if this.Music.current:isStopped( ) then 
			if this.Music.loop then
				this.Music.current = this.Music.queue[queuePos].sound
			else
				if this.Music.queue[queuePos+1] then
					this.Music.queuePos = this.Music.queuePos + 1
					this.Music.current = this.Music.queue[queuePos].sound
					this.Music.current:play()
				else
					this.Music.current = nil
					this.queue = {}
				end
			end
		
		end
	
	else
	
	end

end

------------
function AudioEngine.AddListener(Gameobject)
{
	this = AudioEngine
	
	table.insert(this.listening,Gameobject)
}

--SFX FUNCTIONS--
function AudioEngine.SFX.Play(sound,power)-- power = radius in pixels
{
	this = AudioEngine
	if AudioEngine.files[sound] == nil then
		local newsound = {}
		newsound.sound = love.audio.newSource( sound, "static" )
		AudioEngine.files[sound] = newsound
	end
	for i,v in pairs(this.listening) do
		if v.Transform.position:distance() < power then
			AudioEngine.files[sound].sound:setVolume(v.Transform.position:distance()/power)
			AudioEngine.files[sound].sound:play()
		end
	end
}
function AudioEngine.SFX.PlayIgnore(sound)-- power = radius in pixels
{
	this = AudioEngine
	if AudioEngine.files[sound] == nil then
		local newsound = {}
		newsound.sound = love.audio.newSource( sound, "static" )
		AudioEngine.files[sound] = newsound
	end
	AudioEngine.files[sound].sound:play()
}

function AudioEngine.SFX.StopAll()
{
	for i,v in pairs(AudioEngine.files) do
		v.sound:stop()
	end
}

--MUSIC VARS--
AudioEngine.Music.loop = false
AudioEngine.Music.fadein = false
AudioEngine.Music.fadeout = false
--MUSIC FUNCTIONS--
function AudioEngine.Music.Queue(sound)
{
	this = AudioEngine
	if AudioEngine.files[sound] == nil then
		local newsound = {}
		newsound.sound = love.audio.newSource( sound, "stream" )
		AudioEngine.files[sound] = newsound
	end
	table.insert(AudioEngine.Music.queue,newsound)
}
function AudioEngine.Music.pause()
{
	this = AudioEngine
	this.Music.playing:pause()
	
}
function AudioEngine.Music.resume()
{
	this = AudioEngine
	if this.Music.current:isPaused() then
		this.Music.playing:resume()
	end
}
function AudioEngine.Music.play(sound)-- Interupts queue and plays, after finished it resumes queue
{
	this = AudioEngine
	if AudioEngine.files[sound] == nil then
		local newsound = {}
		newsound.sound = love.audio.newSource( sound, "stream" )
		AudioEngine.files[sound] = newsound
	end
	AudioEngine.files[sound].sound:play
}

