function love.graphics.drawRepeat(image,x,y,width,height)
	sten = love.graphics.newStencil(function()
			love.graphics.rectangle("fill",x,y,width,height)
		end)
	love.graphics.setInvertedStencil(sten)
	iWidth = image:getWidth()
	iHeight = image:getHeight()
	xrep = math.ceil(width/iWidth)
	yrep = math.ceil(height/iHeight)
	for x = 0,xrep do
		for y = 0,yrep do
			love.graphics.draw(image,x*iWidth,y*iHeight);
		end
	end
	love.graphics.setInvertedStencil()

end