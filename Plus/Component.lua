Component = class 
{
	type = 'Component',
	name = '',
	GameObject = nil,
	Transform = nil
}
function Component:__init(self)
	self.name =  string:split(string:split(debug.getinfo(2).source,'/')[table.getn(string:split(debug.getinfo(2).source,'/'))],'.lua')[1]
	
end

function Component:Start()

end
function Component:update(dt)
  
end
function Component:preRender()

end
function Component:postRender()

end
function Component:draw()
  
end
function Component:destroy()

end
function Component:mousepressed(x, y, button)
  
end

function Component:mousereleased(x, y, button)
  
end

function Component:keypressed(key, unicode)
  
end

function Component:keyreleased(key, unicode)
  
end
function Component:OnCollisionEnter(GameObj)

end
function Component:OnCollisionStay(GameObj)

end
function Component:OnCollisionExit(GameObj)

end
function Component:focus(f)
  
end

function Component:quit()
  
end