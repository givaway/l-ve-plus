--- Tserial v1.4, a simple table serializer which turns tables into Lua script
-- @author Taehl (SelfMadeSpirit@gmail.com)
Tserial = {}
TSerial = Tserial	-- for backwards-compatibility

--- Serializes a table into a string, in form of Lua script.
-- @param t table to be serialized (may not contain any circular reference)
-- @param drop if true, unserializable types will be silently dropped instead of raising errors
-- if drop is a function, it will be called to serialize unsupported types
-- if drop is a table, it will be used as a serialization table (where {[value] = serial})
-- @param indent if true, output "human readable" mode with newlines and indentation (for debug)
-- @return string recreating given table
function Tserial.pack(t, drop, indent)
	assert(type(t) == "table", "Can only Tserial.pack tables.")
	local s, indent = "{"..(indent and "\n" or ""), indent and math.max(type(indent)=="number" and indent or 0,0)
	local function proc(k,v, omitKey)
		local tk, tv, skip = type(k), type(v)
		if type(drop)=="table" and drop[k] then k=drop[k] tk=type(k) end
		if tk == "boolean" then k = k and "[true]" or "[false]"
		elseif tk == "string" then if string.format("%q",k) ~= '"'..k..'"' then k = '['..string.format("%q",k)..']' end
		elseif tk == "number" then k = "["..k.."]"
		elseif tk == "table" then k = "["..Tserial.pack(k, drop, indent and indent+1).."]"
		elseif type(drop) == "function" then k = "["..string.format("%q",drop(k)).."]"
		elseif drop then skip = true
		else error("Attempted to Tserial.pack a table with an invalid key: "..tostring(k))
		end
		if type(drop)=="table" and drop[v] then v=drop[v] tv=type(v) end
		if tv == "boolean" then v = v and "true" or "false"
		elseif tv == "string" then v = string.format("%q", v)
		elseif tv == "number" then	-- no change needed
		elseif tv == "table" then v = Tserial.pack(v, drop, indent and indent+1)
		elseif type(drop) == "function" then v = string.format("%q",drop(v))
		elseif drop then skip = true
		else error("Attempted to Tserial.pack a table with an invalid value: "..tostring(v))
		end
		if not skip then return string.rep("\t",indent or 0)..(omitKey and "" or k.."=")..v..","..(indent and "\n" or "") end
		return ""
	end
	-- todo: speed-test to see if the extra processing slows it down very much
	local h
	for i=1,#t do s, h = s..proc(i, t[i], true), i end
	for k, v in pairs(t) do if not (type(k)=="number" and k<=h) then s = s..proc(k, v) end end
	s = string.sub(s,1,string.len(s)-1)
	if indent then s = string.sub(s,1,string.len(s)-1).."\n" end
	return s..string.rep("\t",(indent or 1)-1).."}"
end

--- Loads a table into memory from a string (like those output by Tserial.pack)
-- TODO: If given a serial table, replace serial with var in memory
-- @param s a string of Lua defining a table, such as "{2,4,8,ex='ample'}"
-- @return a table recreated from the given string
function Tserial.unpack(s)
	assert(type(s) == "string", "Can only Tserial.unpack strings.")
	assert(loadstring("Tserial.table="..s))()
	local t = Tserial.table
	Tserial.table = nil
	return t
end