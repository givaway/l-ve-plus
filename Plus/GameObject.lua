InitializingScene = nil
GameObject = class 
{
	Components = {},
	Transform,
	type = 'GameObject',
	Scene =  nil,
	name = "",
	id = 0,
	visible = true,
	destroyed = false,
	persist = false
}
function GameObject:__init(name,initializingComponents)
	self.Transform = Trans:new()
	self.Transform.GameObject = self
	self.Components = initializingComponents or {}
	

	if InitializingScene ~= nil then
		self.Scene = InitializingScene
		table.insert(InitializingScene.GameObjects,self)
		self.id = table.getn(InitializingScene.GameObjects)
		local namecheck = 1
		if InitializingScene:FindGameObject(name) ~= nil then
			while InitializingScene:FindGameObject(name.." "..namecheck) ~= nil do
				namecheck = namecheck + 1
			end
			self.name = name.." "..namecheck
		else
			self.name = name
		end
	end

	for i,v in ipairs(self.Components) do
		
		if v ~= nil then
			v.Transform = self.Transform
			v.GameObject = self
		end
	end
	
		self:Start()
	
end
function GameObject:getType()
	return self.type
end
function GameObject:setType(newType)
	if(self.type ~= newType ) then
		self.type = newType
	end
end
-- all these are the needed functions, you can overwrite if you want but odds are you'll break shit
function GameObject:Start()
	self.Transform:Start()
	for i,v in ipairs(self.Components) do
		v:Start()
	end
end

function GameObject:update(dt)
	
	self.Transform:update(dt)
	for i,v in ipairs(self.Components) do
		v.Transform = self.Transform
		v:update(dt)
		self.Transform = v.Transform
	end
	
end
function GameObject:preRender()
	
	for i,v in ipairs(self.Components) do
		v:preRender()
	end
	
	
end
function GameObject:postRender()
	
	for i,v in ipairs(self.Components) do
		v:postRender()
	end
	
	
end
function GameObject:draw()
	
	for i,v in ipairs(self.Components) do
		v:draw()
	end
	
	
end

function GameObject:mousepressed(x, y, button)
	
  	for i,v in ipairs(self.Components) do
		v:mousepressed(x, y, button)
	end
	
end

function GameObject:mousereleased(x, y, button)

	for i,v in ipairs(self.Components) do
		v:mousereleased(x, y, button)
	end

end

function GameObject:keypressed(key, unicode)

	for i,v in ipairs(self.Components) do
		v:keypressed(key, unicode)
	end

end

function GameObject:keyreleased(key, unicode)
	
	for i,v in ipairs(self.Components) do
		v:keyreleased(key, unicode)
	end

end
function GameObject:OnCollisionEnter(GameObj)
	for i,v in ipairs(self.Components) do
		v:OnCollisionEnter(GameObj)
	end
end
function GameObject:OnCollisionStay(GameObj)
	for i,v in ipairs(self.Components) do
		v:OnCollisionStay(GameObj)
	end
end
function GameObject:OnCollisionExit(GameObj)
	for i,v in ipairs(self.Components) do
		v:OnCollisionExit(GameObj)
	end
end
function GameObject:focus(f)
	
	for i,v in ipairs(self.Components) do
		v:focus(f)
	end

end

function GameObject:quit()
	
	for i,v in ipairs(self.Components) do
		v:quit()
	end

end
function GameObject:Destroy()
	if self.Scene:DestroyGameObject(self.name) then
	
	else
	
	end
	for i,v in ipairs(self.Components) do
		v:destroy()
		v = nil
		
	end
	
	
	
end
-- Component functions
function GameObject:GetComponent(name)
	ret = {}

	for i,v in ipairs(self.Components) do
		if v.name == name then
			table.insert(ret,v)
			
		end
		
	end
	--if GameObject has more than one of the same Component then it returns a table of components
	if table.getn(ret) > 1 then
			return ret
	elseif table.getn(ret) == 1 then
		return ret[1]
	else
		return nil
	end
end
--sees if component exist, GetComponent does this too
function GameObject:ComponentExist(name)
	ret = false
	for i,v in ipairs(self.Components) do
		if v.name == name then
			ret = true
		end
	end
	return ret
end
function GameObject:AddComponent(Component)

	if self:ComponentExist(Component.name) == false then
		Component.GameObject = self
		Component.Transform = self.Transform
		
		table.insert(self.Components,Component)
		Component:Start()
	end
end