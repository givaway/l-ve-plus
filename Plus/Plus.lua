require "Plus/require"

Plus = {}
-- toggles debuggingd
Plus.debugging = true
Plus.speedtest = false -- fix this soon -- 
Plus.screenspacelighting = true

-- (Would NOT recomment overriding any of the below functions)
function Plus:load()
	Resources.load("")
	Resources.include()
end
function love.update(dt)
	CurrentScene:update(dt)
end

function love.draw()
	CurrentScene:draw()
end

-- Other Callback Functions

function love.mousepressed(x, y, button)
	CurrentScene:mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
	CurrentScene:mousereleased(x, y, button)
end

function love.keypressed(key, unicode)
	CurrentScene:keypressed(key, unicode)	
end

function love.keyreleased(key, unicode)
	CurrentScene:keyreleased(key, unicode)
end

function love.focus(f)
	CurrentScene:focus(f)	
end

function love.quit()
	CurrentScene:quit()	
end