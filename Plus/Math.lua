math.lerp = function(a,b,i)
	return (a*(1-i)+b*i);
end
math.cerp = function(a,b,i)
	i2 = (1-math.cos(i*math.pi))/2;
    return (a*(1-i2)+b*i2);
end
math.bezier = function(a,b,c,d,t)
	
	return ((1-t)^3)*a+(3*((1-t)^2))*(t*b)+(3*(1-t))*(t^2)*c+(t^3)*d

end
math.timers = {}
math.timer = function(name,timeout)
	if math.timers[name] == nil then
		math.timers[name] = 0

		return 0
	else
		if math.timers[name] < timeout then
			return math.timers[name]
		else
			math.timers[name] = nil
			return true
		end
	end
end
math.update = function(dt)
	for i,v in pairs(math.timers) do
		math.timers[i] = math.timers[i] + 1000*dt
	end
end