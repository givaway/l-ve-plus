Vector2 = class
{
	x = 0,
	y = 0
}
function Vector2:__init(x,y)
	self.x = x
	self.y = y
end

function Vector2:add(v2)
	x = self.x + v2.x
	y = self.y + v2.y
	return Vector2:new(x,y)
end
function Vector2:subtract(v2)
	x = self.x - v2.x
	y = self.y - v2.y
	return Vector2:new(x,y)
end

function Vector2:multiply(v2)
	x = self.x * v2.x
	y = self.y * v2.y
	return Vector2:new(x,y)
end

function Vector2:divide(v2)
	x = self.x * v2.x
	y = self.y * v2.y
	return Vector2:new(x,y)
end

function Vector2:slope(v2)
	if v2 ~= nil then

		x = v2.x - self.x
		y = v2.y - self.y
		return Vector2:new(x,y)
	else
		return Vector2:new(0,0)
	end
end
function Vector2:distance(v2)
	xDist = math.abs(self.x - v2.x);  
	yDist = math.abs(self.y - v2.y);  
	Distance = math.sqrt((xDist * xDist) + (yDist * yDist))
	return Distance
end
function Vector2:print()
	print(self.x,self.y)
end
function Vector2:equals(v2)
	if self.x == v2.x and self.y == v2.y then
		return true
	else
		return false
	end
end
function Vector2:separate()
	return self.x , self.y
end
function Vector2:Normalized()
	local ret = Vector2:new(0,0)
	if self:length() ~= 0 then
		ret.x = self.x/self:length()
		ret.y = self.y/self:length()
	end
	return ret
end
function Vector2:normalize()
	if self:length() ~= 0 then
		self.x = self.x/self:length()
		self.y = self.y/self:length()
	end
end
function Vector2:length()
	return math.sqrt((x^2)+(y^2))
end
function Vector2:dot(vec)
	return (self.x*vec.x) + (self.y*vec.y)
end
function Vector2:flip()
	return Vector2:new(-self.x,-self.y)
end
function Vector2:angleBetween(vec)
	angle = math.deg(math.atan2(vec.y - self.y , vec.x - self.x))
	if angle < 0 then angle = 360 + angle end
	return math.abs(angle)
end
function Vector2:angleBetweenRad(vec,adds)
	angle = math.deg(math.atan2(vec.y - self.y , vec.x - self.x))
	if angle < 0 then angle = 360 + angle end
	return math.rad(math.abs(angle)+adds)
end