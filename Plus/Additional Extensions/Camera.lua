Camera = class
{
	x = 0,
	y = 0,
	scale = 1,
	rot = 0,
	follower = nil
}
function Camera:__init()

end
function Camera:open()
	if self.follower ~= nil then
		--print(self.follow)
		self.x = self.follower.Transform.position.x
		self.y = self.follower.Transform.position.y
		
	end
	
	local cx , cy = love.graphics.getWidth() / (2 * self.scale ) , love.graphics.getHeight() / ( 2 * self.scale )
	love.graphics.push()
	love.graphics.scale(self.scale)
	love.graphics.translate(cx, cy)
	love.graphics.rotate(math.rad(self.rot))
	love.graphics.translate(-self.x, -self.y)

end
function Camera:close()
	love.graphics.pop()
end
function Camera:asVector()
	return Vector2:new(self.x,self.y)
end
function Camera:follow(GameObj)
	print(GameObj.name)
	self.follower = GameObj
	
end
function Camera:cancelFollow()
	self.follow = nil
end
function Camera:camToWorld(x,y)
	local cx , cy = love.graphics.getWidth() / (2 * self.scale ) , love.graphics.getHeight() / ( 2 * self.scale )
	return Vector2:new((x/self.scale)-cx+self.x, (y/self.scale)-cy+self.y)
end
function Camera:worldToCam(x,y)
	ret= Vector2:new((x-(self.x-(love.window.getWidth()/self.scale)/2))*self.scale,(y-(self.y-(love.window.getHeight()/self.scale)/2))*self.scale)
	return ret
end


