Trans = Component:extends
{
	position = nil,
	Cam = nil,
	z = 0,
	rot = 0,
	zignore = false,
	rotFollow = nil,
	transFollow = nil,
	rotOff = 0
}
function Trans:__init()
	self.position = Vector2:new(0,0)
end
function Trans:Start()
	
end
function Trans:update()
	if self.transFollow ~= nil then
		self.position.x = self.transFollow.Transform.position.x
		self.position.y = self.transFollow.Transform.position.y
	end
	
	if self.rotFollow ~= nil then
		self.rot = self.position:angleBetweenRad(self.rotFollow.Transform.position,self.rotOff)

	end
end
function Trans:follow(obj)
	self.transFollow = obj
end
function Trans:rotateTowards(obj)
	self.rotFollow = obj
end
function Trans:cancelRotation()
	self.rotFollow = nil
end