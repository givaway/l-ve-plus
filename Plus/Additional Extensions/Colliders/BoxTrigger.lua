BoxTrigger = Component:extends
{
	collider = {},
	width = 0,
	height = 0,
	x= 0,
	y = 0,
	ctype = "dynamic",
	rot = 0,
	lockRotation = false
}
function BoxTrigger:__init(isStatic,x,y,width,height,rot)
	self.super.__init(self,self)
	self.width = width
	self.height = height
	self.static = isStatic
	self.x = x
	self.y = y
	if isStatic then
		self.ctype = "static"
	end
	if rot == false then
	self.lockRotation = true
	else
	self.rot = rot or 0
	end
end
function BoxTrigger:lockRot(var)
	self.lockRotation = var
end
function BoxTrigger:Start()
	self.collider = {}
	self.collider.b = love.physics.newBody(self.GameObject.Scene.PhysicsObject,self.x,self.y,self.ctype)
	self.collider.s = love.physics.newRectangleShape(0,0,self.width,self.height,math.rad(self.rot))
	self.collider.f = love.physics.newFixture(self.collider.b,self.collider.s)
	self.collider.f:setSensor( true )
	local data = {}
	data.GameObj = self.GameObject
	data.touching = {}
	self.collider.f:setUserData(data)
	self.Transform.position.x = self.x
	self.Transform.position.y = self.y
	self.lastx = self.collider.b:getX()
	self.lasty = self.collider.b:getY()
	self.Transform.coll = self
	 
end
function BoxTrigger:update(dt)
	if self.collider.b:isAwake() == false then self.collider.b:setAwake(true) end
	self.collider.b:setX(self.collider.b:getX()+(self.Transform.position.x - self.lastx))
	self.collider.b:setY(self.collider.b:getY()+(self.Transform.position.y - self.lasty))
	self.Transform.position.x = self.collider.b:getX()
	self.Transform.position.y = self.collider.b:getY()
	if self.lockRotation then
		self.collider.b:setAngle( self.Transform.rot )
	else
	self.Transform.rot = self.collider.b:getAngle()
	end
	self.lastx = self.Transform.position.x
	self.lasty = self.Transform.position.y
	
end
function BoxTrigger:getPoints()
	return self.collider.b:getWorldPoints(self.collider.s:getPoints())
end
function BoxTrigger:draw()
	if Plus.debugging then
	love.graphics.setLine(2, "smooth")
	love.graphics.setColor({255,200,200,255})
	love.graphics.polygon("line", self.collider.b:getWorldPoints(self.collider.s:getPoints()))
	--love.graphics.print(self.GameObject.name,self.collider.b:getX(),self.collider.b:getY())
	love.graphics.setColor({255,255,255,255})
	love.graphics.setLine(1, "smooth")
	end
end
function BoxTrigger:get()
	return self.collider
end
