BoxCollider = Component:extends
{
	collider = {},
	width = 0,
	height = 0,
	x= 0,
	y = 0,
	ctype = "dynamic",
	rot = 0,
	lockRotation = false,
	destroyed = false
}
function BoxCollider:__init(isStatic,x,y,width,height,rot)
	self.super.__init(self,self)
	self.width = width
	self.height = height
	self.static = isStatic
	self.x = x
	self.y = y
	if isStatic then
		self.ctype = "static"
	end
	if rot == false then
	self.lockRotation = true
	else
	self.rot = rot or 0
	end
end
function BoxCollider:lockRot(var)
	self.lockRotation = var
end
function BoxCollider:destroy()
	if self.destroyed == false then
	self.collider.f:destroy()
	self.collider.f = nil
	self.collider.b:destroy()
	self.collider.b = nil
	
	self.destroyed = true;
	end
end
function BoxCollider:Start()
	self.destroyed = false;
	self.collider = {}
	self.collider.b = love.physics.newBody(self.GameObject.Scene.PhysicsObject,self.x,self.y,self.ctype)
	self.collider.s = love.physics.newRectangleShape(0,0,self.width,self.height,math.rad(self.rot))
	self.collider.f = love.physics.newFixture(self.collider.b,self.collider.s)
	local data = {}
	data.GameObj = self.GameObject
	data.touching = {}
	self.collider.f:setUserData(data)
	self.Transform.position.x = self.x
	self.Transform.position.y = self.y
	self.lastx = self.collider.b:getX()
	self.lasty = self.collider.b:getY()
	self.Transform.coll = self
	 
end
function BoxCollider:update(dt)
	if self.collider.b:isAwake() == false then self.collider.b:setAwake(true) end
	self.collider.b:setX(self.collider.b:getX()+(self.Transform.position.x - self.lastx))
	self.collider.b:setY(self.collider.b:getY()+(self.Transform.position.y - self.lasty))
	self.Transform.position.x = self.collider.b:getX()
	self.Transform.position.y = self.collider.b:getY()
	if self.lockRotation then
		self.collider.b:setAngle( self.Transform.rot )
	else
	self.Transform.rot = self.collider.b:getAngle()
	end
	self.lastx = self.Transform.position.x
	self.lasty = self.Transform.position.y
	
end
function BoxCollider:Recreate()
	self:destroy()
	self:Start()

end
function BoxCollider:getPoints()
	return self.collider.b:getWorldPoints(self.collider.s:getPoints())
end
function BoxCollider:draw()
	if Plus.debugging then
	love.graphics.setLineWidth(2)
	love.graphics.setColor({255,200,200,255})
	love.graphics.polygon("line", self.collider.b:getWorldPoints(self.collider.s:getPoints()))
	--love.graphics.print(self.GameObject.name,self.collider.b:getX(),self.collider.b:getY())
	love.graphics.setColor({255,255,255,255})
	love.graphics.setLineWidth(1)
	end
end
function BoxCollider:get()
	return self.collider
end
