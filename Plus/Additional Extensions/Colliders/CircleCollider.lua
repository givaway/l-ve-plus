CircleCollider = Component:extends
{
	collider = {},
	radius = 5,
	x= 0,
	y = 0,
	ctype = "dynamic",
	rot = 0,
	lastx = 0,lasty = 0
}
function CircleCollider:__init(isStatic,x,y,radius,rot)
	self.super.__init(self,self)
	self.radius = radius
	self.static = isStatic
	self.x = x
	self.y = y
	if isStatic then
		self.ctype = "static"
	end
	self.rot = rot or 0
end
function CircleCollider:Start()
	 self.collider = {}
	 self.collider.b = love.physics.newBody(self.GameObject.Scene.PhysicsObject,self.x,self.y,self.ctype)
	 self.collider.s = love.physics.newCircleShape(0,0,self.radius)
	 self.collider.f = love.physics.newFixture(self.collider.b,self.collider.s)
	 local data = {}
	 data.GameObj = self.GameObject
	 data.touching = {}
	 self.collider.f:setUserData(data)
	 self.Transform.position.x = self.x
	 self.Transform.position.y = self.y
		self.lastx = self.collider.b:getX()
		self.lasty = self.collider.b:getY()
	 
end
function CircleCollider:update(dt)
		self.collider.b:setX(self.collider.b:getX()+(self.Transform.position.x - self.lastx))
		self.collider.b:setY(self.collider.b:getY()+(self.Transform.position.y - self.lasty))
		self.Transform.position.x = self.collider.b:getX()
		self.Transform.position.y = self.collider.b:getY()
		self.Transform.rot = self.collider.b:getAngle()
		self.lastx = self.Transform.position.x
		self.lasty = self.Transform.position.y
	
end
function CircleCollider:getPoints()
	points = {}
	local xp,yp = 0,0
	for i = 0,10 do 
		pr = i/10
		xp = self.Transform.position.x+(math.sin(2*math.pi*pr)*self.radius)
		yp = self.Transform.position.y+(math.cos(2*math.pi*pr)*self.radius)
		table.insert(points,xp)
		table.insert(points,yp)
	end
	return points
end
function CircleCollider:draw()
	if Plus.debugging then
	love.graphics.setLineWidth(2)
	love.graphics.setColor({255,200,200,255})
	love.graphics.polygon("line", self:getPoints())
	--love.graphics.print(self.GameObject.name,self.collider.b:getX(),self.collider.b:getY())
	love.graphics.setColor({255,255,255,255})
	love.graphics.setLineWidth(1)
	end
end
function CircleCollider:get()
	return self.collider
end
