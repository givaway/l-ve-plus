Lighting = Component:extends
{
	polys = {},
	checkingInt = 20,
	cCheck = 0,
	inside = false,
	
}
function Lighting:__init()
	self.super.__init(self,self)
end
function Lighting:Start()
	self.Transform.z = 0
end
function Lighting:update(dt)
			
			self:Screenspace(Plus.screenspacelighting)
end
function Lighting:Screenspace(bool)
	if bool then
		if self.GameObject.visible then
			self:GetShadows()
		end
	else
		self:GetShadows()
	end
end
function Lighting:GetShadows()
			
			self.polys = {}
			local poly = nil
			if self.GameObject:GetComponent("BoxCollider") == nil then
				 poly = self.GameObject:GetComponent("CircleCollider"):getPoints()
			else
				 poly = {self.GameObject:GetComponent("BoxCollider"):getPoints()}
			end
			
			local newPoly = {}
			for i = 1, table.getn(poly),2 do
				table.insert(newPoly,{poly[i],poly[i+1]})
			end
			local lightcount = false
			if self.cCheck > self.checkingInt or self.inside then
				self.cCheck = 0
				for i,v in pairs(CurrentScene.Lights) do
				
					
					local dist = self.Transform.position:distance(v.Transform.position)
					local rads = v:GetComponent("PointLight").radius
					if dist < rads then
						lightcount = true
							local light = v.Transform.position
							local spoly = {}
							for a,b in pairs(newPoly) do
								local vectorpos = Vector2:new(b[1],b[2])
								local vectorslope = vectorpos:slope(light)
								:Normalized()
								:multiply(Vector2:new(99999,99999))
								local newpos = vectorpos:subtract(vectorslope)
								table.insert(spoly,{newpos.x,newpos.y})
							end
							self.polys[v.name] = {}
							for i = 1, table.getn(newPoly) do
								if newPoly[i+1] ~= nil then
									local lightpoly = {
									newPoly[i][1],
									newPoly[i][2],
									newPoly[i+1][1],
									newPoly[i+1][2],
									spoly[i+1][1],
									spoly[i+1][2],
									spoly[i][1],
									spoly[i][2]
									}
									
									 table.insert(self.polys[v.name],lightpoly)
								else
									local lightpoly = {
									newPoly[i][1],
									newPoly[i][2],
									newPoly[1][1],
									newPoly[1][2],
									spoly[1][1],
									spoly[1][2],
									spoly[i][1],
									spoly[i][2]}
									table.insert(self.polys[v.name],lightpoly)
								end
							
							end
						table.insert(v:GetComponent("PointLight").LightPolys,self.GameObject)
					end
				end
			end
			self.cCheck = self.cCheck + 1
			self.inside = lightcount
end
