extern vec2 position = vec2(0,0);
extern number radius = 0;
extern vec2 screen = vec2(0,0);
extern number scale = 1;
uniform number hard = 1;
vec4 effect(vec4 colour, Image tex, vec2 tc, vec2 sc)
{

	number xDist = abs(sc.x-position.x);  
	number yDist = abs(screen.y-sc.y-position.y);    
	number distance = floor(sqrt((xDist * xDist) + (yDist * yDist))/scale);
	number power = 0;
	if(hard)
	{
		power = (radius/distance);

		power *= 0.06;
	}
	else
	{
		power = 1-(distance/radius);
		
	}
	return colour*power;
}