PointLight = Component:extends
{
	radius = 50,
	color = {255,255,255,128},
	LightPolys = {},
	lightingMask = nil,
	enabled = true
}
function PointLight:__init(x,y,color,radius,enabled,hard)
self.super.__init(self,self)
	self.radius = radius or 50
	self.color = color
	self.x = x
	self.y = y
	self.enabled = enabled
	self.hard = hard
	
end
function PointLight:Start()
	self.GameObject.persist = true
	CurrentScene.Lights[self.GameObject.name] = self.GameObject
	self.shader = love.graphics.newShader(Resources.effect["light.pe"])
	print(self.shader)
	self.Transform.position.x = self.x
	self.Transform.position.y = self.y
	self.Transform.z = 50
	self.lightingMask = function()
			for i,v in pairs(self.LightPolys) do
				local obj = v:GetComponent("Lighting")
				if obj.polys[self.GameObject.name] ~= nil then
					for a,b in pairs(obj.polys[self.GameObject.name]) do
						love.graphics.polygon("fill",b)
					end
				end
			end
		end
	
end

function PointLight:update(dt)
	

end
function PointLight:draw()
	if self.enabled then
	love.graphics.setColor(self.color)
	love.graphics.setBlendMode("additive")
	love.graphics.setShader( self.shader )
	self.shader:send("position", {MainCamera():worldToCam(self.Transform.position.x,self.Transform.position.y).x,MainCamera():worldToCam(self.Transform.position.x,self.Transform.position.y).y})
	self.shader:send("screen", {love.window.getWidth(),love.window.getHeight()})
	self.shader:send("radius", self.radius)
	self.shader:send("scale", MainCamera().scale)
	if self.hard then 
		self.shader:send("hard", 1)
	else
		self.shader:send("hard", 0)
	end
	if table.getn(self.LightPolys) > 0 then
	
	love.graphics.setInvertedStencil(self.lightingMask)
	end

	love.graphics.circle( "fill", self.Transform.position.x, self.Transform.position.y, self.radius, 20)
	love.graphics.setInvertedStencil()
	
	love.graphics.setShader( pixeleffect )
	love.graphics.setBlendMode("alpha")
	end
	self.LightPolys = {}
	
end